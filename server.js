const express = require("express");
const app = express();
const { PORT = 8000 } = process.env;
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var corsOptions = {
  origin: "http://localhost:8081",
};

app.use(cors());

const passport = require("./lib/passport");
app.use(passport.initialize());

const router = require("./router");
app.use(router);
app.listen(PORT, () => {
  console.log(`Server nyala di port ${PORT}`);
});
