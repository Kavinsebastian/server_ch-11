const { User } = require("../models");
function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  register: (req, res, next) => {
    User.register(req.body)
      .then(() => {
        res.json({ message: "anda berhasil daftar" });
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "Error bosq, masukin data yang benar",
        });
      });
  },
  login: (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.json(format(user));
      })
      .catch((err) => {
        res.status(500).json({
          result: "FAILED",
          message: err.message || "Error bosq, masukin data yang benar",
        });
      });
  },
  profil: (req, res) => {
    const currentUser = req.user;
    res.json(currentUser);
  },
};
