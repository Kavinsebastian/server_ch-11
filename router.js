
const router = require("express").Router();
const auth = require("./controllers/authController");
const restrict = require("./middlewares/restrict");

router.post("/api/auth/register", auth.register);
router.post("/api/auth/login", auth.login);
router.get("/api/auth/profil", restrict, auth.profil);

module.exports = router;
